import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;


public class CachingHTTPClient {
	static SimpleDateFormat timeFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");

	public static void main(String args[]) {
		
		if (args.length < 1) {
			System.out.println("Usage:");
			System.out.println("java CachingHTTPClient <url>");
			System.exit(0);
		}
		URL url = null;
		String inputUrl = args[0].replace("/", ";");
		File folder = new File("/tmp/ck8555/assignment1/");
		folder.mkdirs();
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile()){
		    	String[] fileName = file.getName().split("-");	
		    	String fileExpiresTime = fileName[1].replace(".txt", "");
		    	if( fileName[0].compareTo(inputUrl) == 0) {       //compares url with file name and current time with expire time in sys milli
		    		if(System.currentTimeMillis() <= Long.parseLong(fileExpiresTime)) {
		    			System.out.println("***** Serving from the cache – start *****");
				        Scanner file1 = null;
				        Scanner file2 = null;
						try {
							file1 = new Scanner(file);
							file2 = new Scanner(file);
							file1.findWithinHorizon("Date : ", 5000);
							String date = file1.nextLine();
							date = date.replace("[", "");
							date = date.replace("]", "");
	
							Date gotDate = timeFormat.parse(date);
							String n = "";
							
					        while ( (n = file2.nextLine()).length() > 0)
					        {
					        	System.out.println(n);
					        	if(n.contains("OK]")){
									System.out.println("Age : [" + ((System.currentTimeMillis()/1000)-(gotDate.getTime()/1000)) + "]");
					        	}
					        }
						} catch (IOException | ParseException e) {
							e.printStackTrace();
						} finally {
							if (file1 != null)
								file1.close();
							if (file2 != null)
								file2.close();
					        System.out.println("***** Serving from the cache – end *****");
					        System.exit(0);
						}
		    		}
		    		else {
		    			System.out.println("Expired Cache file deleted");
		    			file.delete();
		    		}
	    		}
		    }
		}
		 
		try {
			url = new URL(args[0]);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		assert url != null;
		
		try {
			URLConnection connection = url.openConnection();
			boolean ifChunked = false;
			//include url error handling e.g. content = 404
			int responseCode = ((HttpURLConnection)connection).getResponseCode();
			if(responseCode/400 >= 1){
				System.out.println("Invaild Url Error code: " + responseCode);
				System.out.println(((HttpURLConnection)connection).getErrorStream());
				System.exit(0);
			}
			String encoding = connection.getHeaderField("Transfer-Encoding");
			long responseDate = connection.getDate();
			long responseExpires = connection.getExpiration();
			String responseCacheControl =  connection.getHeaderField("Cache-Control");	
			long responseLastModified =  connection.getLastModified();				
			long expiresDate = 	responseDate;	
			if (responseCacheControl.contains("s-maxage")){
				String[] rawParam = responseCacheControl.split("=");
				String[] rightParam = rawParam[1].split(",");
				long sMaxTime = Integer.parseInt(rightParam[0]);  //in seconds
				expiresDate = expiresDate + (sMaxTime * 1000);
			}
			else if (responseCacheControl.contains("max-age")){
				String[] rawParam = responseCacheControl.split("=");
				String[] rightParam = rawParam[1].split(",");
				long maxageTime = Integer.parseInt(rightParam[0]); // in seconds
				expiresDate = expiresDate + (maxageTime * 1000);

			}
			else if (responseExpires != 0){
				expiresDate = responseExpires;
			}
			else{
				// Heuristic approach "now + (now - lastModified)/10"    assumption: if the response header doesn't have a expires tag then there will definitely be a last-modified tag 
				if(responseLastModified != 0){
					long now = expiresDate;
					expiresDate = now + ((now - responseLastModified)/10);
				}
			}
			//Create files in format {url}-{Expires in milliseconds}
			if(!(responseDate == 0 || expiresDate == responseDate)){
				String fileName = inputUrl+ "-" + expiresDate + ".txt";
				File file = new File("/tmp/ck8555/assignment1/" + fileName);

				file.getParentFile().mkdirs();

				FileOutputStream writer = new FileOutputStream(file);
				PrintStream printer = new PrintStream(writer);

				System.out.println("***** Serving from the source – start *****");
				Map headerfields = connection.getHeaderFields();
				Set headers = headerfields.entrySet(); 
				for(Iterator i = headers.iterator(); i.hasNext();){ 
					Map.Entry map = (Map.Entry)i.next();
					System.out.println(map.getKey() + " : " + map.getValue() + ""); 
					printer.println(map.getKey() + " : " + map.getValue() + "");
				}
				InputStream input = connection.getInputStream();
				byte[] buffer = new byte[4096];
				int n = - 1;

				while ( (n = input.read(buffer)) != -1)
				{
					if (n > 0)
					{
						System.out.write(buffer, 0, n);
						writer.write(buffer, 0, n);
					}
				}			
				writer.close();
				System.out.println("***** Serving from the source – end *****");
			}
			else{
				System.out.println("***** Serving from the source – start *****");
				Map headerfields = connection.getHeaderFields();
				Set headers = headerfields.entrySet(); 
				for(Iterator i = headers.iterator(); i.hasNext();){ 
					Map.Entry map = (Map.Entry)i.next();
					System.out.println(map.getKey() + " : " + map.getValue() + ""); 
				}
				InputStream input = connection.getInputStream();
				byte[] buffer = new byte[4096];
				int n = - 1;

				while ( (n = input.read(buffer)) != -1)
				{
					if (n > 0)
					{
						System.out.write(buffer, 0, n);
					}
				}			
				System.out.println("***** Serving from the source – end *****");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
